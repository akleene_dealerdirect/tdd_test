<?php

class WordWrapper implements RopBot\WordWrapInterface
{
    /**
     * @var int|null
     */
    private $maxLength;

    /**
     * WordWrapper constructor.
     *
     * @param int|null $p_max_length
     */
    public function __construct($p_max_length = null)
    {
        if ($p_max_length !== null) {
            $this->maxLength = $p_max_length;
        }
    }

    /**
     * @param string $text
     * @return string mixed
     */
    public function wrap($text)
    {
        if (!is_string($text)) {
            throw new InvalidArgumentException('Argument must be a string');
        }

        if ($this->maxLength === null) {
            $result = $text;
        } else {
            $lines = $this->createLines($text, $this->maxLength);

            $result = implode("\n", $lines);
        }

        return $result;
    }

    /**
     * @param string $text
     * @param int $maxLength
     *
     * @return array
     */
    private function createLines($text, $maxLength)
    {
        $result = array();

        while (strlen($text) > $maxLength) {
            $maxLine = substr($text, 0, $maxLength);
            $lastSpace = strrpos($maxLine, ' ') + 1;
            $result[] = substr($text, 0, $lastSpace);
            $text = substr($text, $lastSpace);
        }

        if ($text !== '') {
            $result[] = $text;
        }

        return $result;
    }
}
