<?php

/**
 * Class WordWrapperTest
 *
 * @coversDefaultClass WordWrapper
 * @covers ::<!public>
 * @covers ::__construct
 */
class WordWrapperTest extends PHPUnit_Framework_TestCase
{
    /**
     * @covers ::wrap
     */
    public function testWrapWithoutLengthInConstructor()
    {
        $wrapper = new WordWrapper();

        $text = 'No length specified';

        $this->assertSame($text, $wrapper->wrap($text));
    }

    /**
     * @param $text
     * @param $length
     * @param $expected
     *
     * @dataProvider wrapperTextProvider
     *
     * @covers ::wrap
     */
    public function testWrap($text, $length, $expected)
    {
        $wrapper = new WordWrapper($length);

        $actual = $wrapper->wrap($text);

        $this->assertSame($expected, $actual);
    }

    public function testWrapThrowsExceptionWhenArgumentNotString()
    {
        $this->setExpectedException(\InvalidArgumentException::class);

        $wrapper = new WordWrapper();

        $wrapper->wrap(new stdClass());
    }

    /**
     * @return array
     */
    public function wrapperTextProvider()
    {
        return [
            'no max length' => ['No max length', null, 'No max length'],
            'below max length' => ['Below max length', 80, 'Below max length'],
            '2 lines' => ['Very long text that needs two lines', 20, "Very long text that \nneeds two lines"],
        ];
    }
}
